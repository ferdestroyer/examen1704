package persistance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import models.User;

public class UserDAO {

    public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost/examen1703";
    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "root";
    private Connection connection = null;
    
    public UserDAO() {
        try {
            Class.forName(DB_DRIVER).newInstance();
        } catch (Exception ex) {
            System.out.println("no se ha cargado el driver");
        }
        try {
            connection = DriverManager.getConnection(DB_URL + "?user=" + DB_USER + "&password=" + DB_USER);
            System.out.println("Conectado");

        } catch (SQLException ex) {
            System.out.println("Fallo Conexion");
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (connection != null) {
            connection.close();        
        }
    }

    public ArrayList <User> all() throws SQLException{
        ArrayList<User> users = new ArrayList<User>();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM users";

        try {
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery();
            System.out.println("consulta realizada");
            while (rs.next()) {
            	User user = new User();
            	user.setName(rs.getString("name"));
            	user.setLogin(rs.getString("login"));
            	user.setPassword(rs.getString("password"));
            	user.setProvinceid(rs.getInt("province_id"));
            	user.setId(rs.getInt("id"));
            	users.add(user);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }             
        
        
        return users;
    }
    
    public void insert(User user) throws SQLException{
        PreparedStatement stmt = null;

        String sql = "INSERT INTO users (name, login, password, province_id) VALUES (?, ?, ?, ?);";

        try {
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, user.getName());
            stmt.setString(2, user.getLogin());
            stmt.setString(3, user.getPassword());
            stmt.setInt(4, user.getProvinceid());
            stmt.executeUpdate() ;
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {}
            }
        }
    }
    
    public static boolean validate(User user){  
    	boolean status=false;
    	PreparedStatement stmt = null;
    	try{  
    	  
    		String sql ="select * from users where email=? and pass=?;";  
    	  
    		stmt.setString(1,user.getLogin());  
    		stmt.setString(2, user.getPassword());  
    	              
    	ResultSet rs=stmt.executeQuery();  
    	status=rs.next();  
    	              
    	}catch(Exception e){}  
    	  
    	return status;  
    	  
    	}  
    	  

}
