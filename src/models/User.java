package models;

public class User {

	public String name;
	public String login;
	public String password;
	public Integer id;
	public Integer provinceid;
	
	
	public User(){
		
	}


	public User(String name, String login, String password, Integer provinceid) {
		super();
		this.name = name;
		this.login = login;
		this.password = password;
		this.provinceid = provinceid;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getProvinceid() {
		return provinceid;
	}


	public void setProvinceid(Integer provinceid) {
		this.provinceid = provinceid;
	}
	
	
	
	
}

