package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.User;
import persistance.UserDAO;

/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/users/*")
public class UsersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsersServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String[] pieces = getUriPieces(request);

        int length = pieces.length;
        
        if (length == 3 || pieces[3].equals("index")) {
        	System.out.println(length);
            // "/index"
            index(request, response);
        } else if(length > 3){
        	String piece = pieces[3];
        	if (piece.equals("create")) {
              create(request, response);
          } 
        	else {
            int id = 0;
            try {
                id = Integer.parseInt(piece);
            } catch (Exception e) {
                response.sendRedirect(request.getContextPath());
            }
            if (length == 4) {
                
            } else {
                piece = pieces[4];
                if (piece.equals("remember")) {
                    //remember(request, response, id);                        
                } else if (piece.equals("delete")){
                    //delete(request, response, id);                        
                } else {
                    response.sendRedirect(".");
                }
            }

        }
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String[] pieces = getUriPieces(request);

        int length = pieces.length;
        if (length == 3) {
            store(request, response);
        }
	}
	
	private void index(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/users.jsp");
        
        ArrayList<User> users = null;
        try {
            UserDAO dao= new UserDAO() ;
            users = dao.all();
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
        }
        
        request.setAttribute("users", users);
        dispatcher.forward(request, response);
    }
	
	private void create(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/create.jsp");
        dispatcher.forward(request, response);
    }
	
	private void store(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = new User(request.getParameter("name"), request.getParameter("login"), request.getParameter("password"),Integer.parseInt(request.getParameter("provinceid")));
        
        try{
        	UserDAO dao = new UserDAO();
            dao.insert(user);
        }catch (SQLException e) {
        	System.out.println("SQLException: " + e.getMessage());
		}
        response.sendRedirect(request.getContextPath() + "/users");
    }
	
	private String[] getUriPieces(HttpServletRequest request) {
        String uri = request.getRequestURI();
         String[] pieces = uri.split("/");
        int i = 0;
        for (String piece : pieces) {
            i++;
        }
        return pieces;
    }

}
