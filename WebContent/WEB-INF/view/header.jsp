<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="/Examen1702/css/default.css">
<title>Recuperación Java EE</title>
</head>
<body>


  <header>

    <div id="title">Recuperación Java EE. Curso 2017/2018</div>
    <nav>
      <ul>
        <li><a href="/Examen1702/">Inicio</a></li>
        <li><a href="/Examen1702/users">Lista de usuarios</a></li>
        <li><a href="/Examen1702/users/create">Nuevo</a></li>
      </ul>
    </nav>

    <nav style="float: right;">
      <ul>
        <li><a href="/Examen1702/login">Login</a></li>
      </ul>
    </nav>    
  </header>
