<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="models.*"%>
<%@ include file = "header.jsp" %>
  <main>
  
<h1>Usuarios</h1>

<%! List <User> users;%>

<%
  users = (List<User>)request.getAttribute("users");
  if (users == null) {
	  users = new ArrayList<User>();
  }
%>

<table border = 1>
<tr>
	<th>Id</th>
	<th>Nombre</th>
	<th>Usuario</th>
	<th>Contraseņa</th>
	<th>Acciones</th>
</tr>
<% for (User item : users) {%>
  <tr>
  <td><%= item.getId() %></td>
  <td><%= item.getName() %></td>
  <td><%= item.getLogin() %></td>
  <td><%= item.getPassword() %></td>
  <td>
	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>">ver</a>
<%--   	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>/edit">editar</a> --%>
	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>/delete">borrar</a>
	<a href="<%= request.getContextPath() %>/movies/<%= item.getId() %>/remember">Recordar</a>

  </td>
  </tr>
<% } %>
</table>
  </main>
<%@ include file = "footer.jsp" %>